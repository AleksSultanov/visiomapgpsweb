"""Модуль для запуска API"""
import uvicorn

_HOST = "127.0.0.1"
_PORT = 8000

if __name__ == "__main__":
    uvicorn.run("server.app:app", host=_HOST, port=_PORT, reload=True)


