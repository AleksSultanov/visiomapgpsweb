"""
  version="1.0.0"
  Логирование визуализации треков на карте
"""

from datetime import datetime

_PRINT_STATUS_ERR = True
_LOG        = []
_ISWEB      = True
_FONTLOG    = "white"
_FONTLOGE   = "red"
_FONTLOGI   = "green"

def _save(_text,level:str=""):
    _date = str(datetime.now())
    if _PRINT_STATUS_ERR:
        print(f"{_date} {_text}")
    if level == "error":
        fontcolor = _FONTLOGE
    elif level == "info":   
        fontcolor = _FONTLOGI 
    else:    
        fontcolor = _FONTLOG   
    if _ISWEB:
        msg = {"datetime":_date,"color":fontcolor,"text":_text,"level":level}
        _LOG.append(msg)

