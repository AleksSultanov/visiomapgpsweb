""" setformW
    version="2.0.0"
    Модуль с классом настроек для фронта

    Облегченная версия setform

"""

import os

def checkbool(s):
    if s == 1:
        return 'checked'
    else:
        return ''  

def boolcheck(_list={},_key=""):
    if _key in _list.keys():
        return 1
    return 0                    

class SetForm:
    """ Класс Формы с настройками
        Свойства /n
        modalResult - Результат открытия формы (Ok,Cancel)
        values - Словарь со значениями
    """
    def __init__(self,name:str='form') -> None:
        self.__savefile = f'.{name}'
        self.__MapLayerfile = f'.MapLayer'
        self.__Defvalues =  {
                "zoom": '10', 
                "fotoOn":'0', 
                "wheatherShow":'1',
                "autoOn":'1',
                "mouseposOn":'0',
                "minimapOn":'0',
                "MapLayerbox":{},
                "tzh":'0',
                "tzmin":'0',
                "sendtg":'0'
                }
        if os.path.exists(self.__savefile):
            with open(self.__savefile, 'r') as openfile: 
                _data=openfile.read()   
                self.__values = eval(_data)
                for _val in self.__Defvalues:
                    if self.__values.get(_val) == None: 
                        self.__values[_val] = self.__Defvalues[_val] 

        else:
            self.__values = self.__Defvalues
        if os.path.exists(self.__MapLayerfile):
            with open(self.__MapLayerfile, 'r') as openfile: 
                _data=openfile.read()   
                self.__MapLayerChooseBox = eval(_data)
        else:
            self.__MapLayerChooseBox =  {}  
    @property
    def mapLayerList(self):
        """Список карт для фронта, с признаком начального выбора
        """
        for x in self.__MapLayerChooseBox:
            if x in self.__values['MapLayerbox']:
                self.__MapLayerChooseBox[x]['defchecked'] = 'checked'
            else:
                self.__MapLayerChooseBox[x]['defchecked'] = ''
        return self.__MapLayerChooseBox
    @property
    def values(self):
        result = self.__values
        result['fotoOnW']       = checkbool(result['fotoOn'])
        result['autoOnW']       = checkbool(result['autoOn'])
        result['wheatherShowW'] = checkbool(result['wheatherShow'])
        result['mouseposOnW']   = checkbool(result['mouseposOn'])
        result['minimapOnW']    = checkbool(result['minimapOn'])
        result['sendtgW']    = checkbool(result['sendtg'])    
        return result

    def savevalues(self,newval):
        self.__values['MapLayerbox'] = {}
        for _key in newval:
            if _key in self.__values.keys():
               self.__values[_key] = newval[_key] 
            if _key.find("mapLayer_") == 0:
                _keyM = _key[9::]
                self.__values['MapLayerbox'][_keyM] = self.__MapLayerChooseBox[_keyM]
        self.__values['fotoOn']    = boolcheck(newval,'fotoOnW')
        self.__values['autoOn']    = boolcheck(newval,'autoOnW')
        self.__values['wheatherShow'] = boolcheck(newval,'wheatherShowW')
        self.__values['mouseposOn']   = boolcheck(newval,'mouseposOnW')
        self.__values['minimapOn']    = boolcheck(newval,'minimapOnW')   
        self.__values['sendtg']    = boolcheck(newval,'sendtgW')  
        print('СОХРАНЕНИЕ НАСТРОЕК',str(self.__values)) 
        with open(self.__savefile, 'w') as savefile:
            savefile.write(str(self.__values))