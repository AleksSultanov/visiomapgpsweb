""" reqGeoData
    version="1.0.0"
    Модуль для методов получения географических данных по сторонним сервисам

"""

import requests

def geoPlace(lat:str,lon:str,isfull:bool=False)->tuple :
    URL = "https://nominatim.openstreetmap.org/reverse"
    _params = {"lat":float(lat),"lon":float(lon),"format":"jsonv2"}
    response = requests.get(URL, params=_params, timeout=30)
    response_status = response.status_code
    if response_status == 200:
        try:
            answer = response.json()
            if len(answer) == 0:
                return str(response_status),"Адрес не определен",{}
            for key in answer:
                if key == 'error':
                    return '99',"Ошибка поиска",answer

            name = answer["display_name"]
            _addres = answer["address"]
            res_dict = {"display_name":name,
                        "address":_addres
                        }
            if isfull:
                res_dict = answer
            return str(response_status),name,res_dict
        except Exception as e:
            return str(response_status),str(e),{}
    else:
        return str(response_status),"error",{}