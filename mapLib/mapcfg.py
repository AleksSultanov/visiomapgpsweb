"""
  version="1.1.0"
  Настройки визуализации треков на карте
"""

_COLORS = ['cadetblue','lightblue','blue','darkblue', 'lightgreen','green', 'darkgreen',
           'lightred','red', 'darkred', 'lightgray','gray','darkgray',
          'lighturple','purple', 'darkpurple',
           'orange',  'beige', 'white',  'black','pink','yellow']

_ICONS =["car", "truck", "sort-up", "male", "fire", "compass",
     "play-circle", "stop-circle", "fighter-jet", "train", "subway", "motorcycle", "bicycle",
     "bus", "map-marker", "plane", "ship", "camera", "camera-retro", 
     "cloud", "bolt", "umbrella"]

_LINECOLORS = ['cadetblue','blue','darkblue', 
               'lightgreen','green', 'darkgreen',
               'red', 'darkred', 
               'gray','darkgray',
               'purple','orange', 'white',  'black','pink','yellow']

def reedAttr():
    # https://fontawesome.com
    # car truck  sort-up male fire compass
    # play-circle stop-circle fighter-jet train subway motorcycle bicycle
    # bus map-marker plane ship camera camera-retro 
    # cloud bolt umbrella
    # https://getbootstrap.com/docs/3.3/components/

    # 'orange', 'darkpurple', 'darkblue', 'lightblue', 'darkgreen', 'red', 
    # 'lightgreen', 'lightred', 'lightgray', 'cadetblue', 'green', 'purple',
    #  'beige', 'white', 'gray', 'blue', 'black', 'darkred', 'pink'.


    Defrow = { "line_color":"blue",
        "line_fill_color" :"lightblue",
        "line_weight":4.5,
        "line_opacity":.5,
        "icon_color":"blue",
        "icon_fill_color":"lightblue",
        "icon_prefix" :"glyphicon",
        "icon_icon":"info-sign",
        "icon_size":None,
        "icon_shadow_size": None,
        "circle_radius": 5,
        "circle_color":"blue" ,
        "circle_fill_color" :"lightblue",
        "circle_fill_opacity" :.5,
        "caption":"",
        "caption_gr":""
        }
    emptyRow = Defrow.copy() 
    emptyRow["icon_icon"] = "map-marker"
    emptyRow["icon_prefix"] = "fa"
    emptyRow["icon_size"] = [5,5]
    emptyRow["icon_shadow_size"] = [5,5]  
    emptyRow["caption_gr"] = '<font color="blue">Треки</font>'    
    # man
    manRow = emptyRow.copy()
    manRow["icon_icon"] = "male"
    manRow["icon_prefix"] = "fa"
    manRow["icon_size"] = [5,5]
    manRow["icon_shadow_size"] = [5,5]  
    autoRow = Defrow.copy()
    autoRow["line_color"]="red"
    autoRow["line_fill_color"]="lightred"
    autoRow["line_weight"] = 4
    autoRow["circle_color"] = "red"
    autoRow["circle_fill_color"] = "lightred"
    autoRow["icon_color"] = "black"
    autoRow["icon_fill_color"] = "black"
    autoRow["icon_icon"] = "car"
    autoRow["icon_prefix"] = "fa"
    autoRow["icon_size"] = [5,5]
    autoRow["icon_shadow_size"] = [5,5]
    autoRow["caption"] = '<font color="red"> <i> авто </i></font>'
    autoRow["caption_gr"] = '<font color="red">Поездки на машине</font>'    
    # motorcycle
    motorcycleRow = autoRow.copy()
    motorcycleRow["icon_icon"] = "motorcycle"
    motorcycleRow["caption"]='<font color="red"> <i> мото </i></font>'
    motorcycleRow["caption_gr"] = '<font color="red">Поездки на мото</font>'        
    #bicycle
    bicycleRow = autoRow.copy()
    bicycleRow["icon_icon"] = "motorcycle"
    bicycleRow["caption"]='<font color="red"> <i> вело </i></font>'
    bicycleRow["caption_gr"] = '<font color="red">Поездки на вело</font>'        
    # bus
    busRow = autoRow.copy()
    busRow["icon_icon"] = "bus"
    busRow["caption"]='<font color="red"> <i> автобус </i></font>'
    busRow["caption_gr"] = '<font color="red">На автобусе</font>'
    # boat
    boatRow = autoRow.copy()
    boatRow["icon_icon"] = "ship"
    boatRow["caption"]='<font color="red"> <i> лодка </i></font>'
    busRow["caption_gr"] = '<font color="red">На лодке</font>'
    # train
    trainRow = emptyRow.copy()
    trainRow["line_color"]="black"
    trainRow["line_fill_color"]="lightblack"
    trainRow["line_weight"] = 4
    trainRow["circle_color"] = "black"
    trainRow["circle_fill_color"] = "lightblack"
    trainRow["icon_color"] = "darkblue"
    trainRow["icon_fill_color"] = "black"
    trainRow["icon_icon"] = "train"
    trainRow["caption"] = '<font color="black"> <i> поезд </i></font>'
    trainRow["caption_gr"] = '<font color="black">Поезд</font>'
    # subway
    subwayRow = autoRow.copy()
    subwayRow["line_weight"] = 4
    subwayRow["icon_icon"] = "subway"
    subwayRow["icon_prefix"] = "fa"
    subwayRow["icon_color"] = "darkred"
    subwayRow["caption"]='<font color="red"> <i> метро </i></font>'
    subwayRow["caption_gr"]='<font color="red">Метро </font>'
    # ship
    shipRow = trainRow.copy()
    shipRow["icon_icon"] = "ship"
    shipRow["caption"]='<font color="red"> <i> корабль </i></font>'
    shipRow["caption_gr"]='<font color="red">На корабле</font>'
    # weather
    weatherRow = Defrow.copy()
    weatherRow["icon_icon"] = "umbrella"
    weatherRow["icon_prefix"] = "fa"
    weatherRow["icon_color"] = "purple"
    weatherRow["icon_fill_color"] = "blue"
    weatherRow["caption_gr"]='<font color="purple">Прогноз погоды</font>'
    # foto
    fotoRow = Defrow.copy()
    fotoRow["icon_icon"] = "camera"
    fotoRow["icon_prefix"] = "fa"
    fotoRow["icon_color"] = "darkred"
    fotoRow["icon_fill_color"] = "lightgreen"
    fotoRow["caption_gr"]='<font color="black">Фотографии</font>'
    # timezone
    timezoneRow = {"hours":0,"minutes":0}
    
    return {"empty":emptyRow,
        "auto":autoRow,
        "man":manRow,
        "train":trainRow,
        "subway":subwayRow,
        "moto":motorcycleRow,
        "bike":bicycleRow,
        "bus":busRow,
        "boat":boatRow,
        "ship":shipRow,
        "weather":weatherRow,
        "foto":fotoRow,
        "timezone":timezoneRow
        }    

def colorlist():
  return _COLORS  

def colorlinelist():
  return _LINECOLORS

def iconslist():
  return _ICONS