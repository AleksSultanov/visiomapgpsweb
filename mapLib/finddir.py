"""
    version="1.1.0"
    Модуль для методов работы с директориями карты

"""

import os
import traceback
from dotenv import load_dotenv
from mapLib.mapcfg import reedAttr, colorlist
from mapLib.mapparse import attrFile

_FILESETGPX = "setting.json"
_FILEPOINTGPX = "userpoint.json"
_FILERESFOTO = "result.csv"
_MAPPREFIX   =  "map" 

#Загрузка переменных
def load_env():
    # dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
    dotenv_path = '.env'
    if os.path.exists(dotenv_path):
        load_dotenv(dotenv_path)


def getMapDirectory(startDir:str = ''):
    if not os.path.isdir(startDir):
        sMes = f'Нет каталога по указанному пути "{startDir}"'
        print(sMes)
        return sMes 
    startDirList = os.listdir(startDir)
    MapDirectory = {}
    LocalSetting = {}
    for d in startDirList:
        if os.path.isdir(d) and (d.upper().find("GPX") == 0 or d.upper().find("FOTO") == 0) :
            mapName = _MAPPREFIX+d[3:]
            dirType = 'GPX'
            if d.upper().find("FOTO") == 0:
                mapName = "map"+d[4:]
                dirType = 'FOTO'
            if mapName not in  MapDirectory.keys():
                MapDirectory.setdefault(mapName,{d:{"files":{},"rootdir":startDir,"dirtype":dirType}})
            else:
                MapDirectory[mapName].setdefault(d,{"files":{},"rootdir":startDir,"dirtype":dirType})

    a = reedAttr()  
    idxMap = 0 
    for _mapName in MapDirectory:
        for _dirName in MapDirectory[_mapName]:
            idxMap +=1
            lstdir  = os.listdir(_dirName) 
            idx = 0 
            fileset = os.path.join(_dirName,_FILESETGPX)
            if os.path.exists(fileset):
                with open(fileset, 'r') as openfile:
                    _data=openfile.read()   
                    LocalSetting = eval(_data)
            for l in lstdir:
                if l in [_FILERESFOTO, _FILESETGPX, _FILEPOINTGPX]:
                    continue 
                idx +=1
                _row = attrFile(a,l) 
                _row["id"] = f"map{idxMap}file{idx}"
                if LocalSetting.get(l) != None:
                    if LocalSetting[l].get("type") != None:
                        _row["type"] = LocalSetting[l]["type"]
                    if LocalSetting[l].get("date") != None:
                        _row["date"] = LocalSetting[l]["date"]
                    if LocalSetting[l].get("name") != None:
                        _row["name"] = LocalSetting[l]["name"]
                    if LocalSetting[l].get("line_color") != None:
                        _row["line_color"] = LocalSetting[l]["line_color"]
                    if LocalSetting[l].get("icon_color") != None:
                        _row["icon_color"] = LocalSetting[l]["icon_color"]
                    if LocalSetting[l].get("line_weight") != None:
                        _row["line_weight"] = float(LocalSetting[l]["line_weight"])

                if _dirName.upper().find("FOTO") == 0:
                    _row["type"] = "foto"
                if _row["type"] in MapDirectory[_mapName][_dirName]["files"].keys():    
                    MapDirectory[_mapName][_dirName]["files"][_row["type"]].append(_row)
                else:
                    MapDirectory[_mapName][_dirName]["files"].setdefault(_row["type"],[_row])  
    return MapDirectory     

def getMapListDirectory(_dirName:str = '',mapname:str='map'):
    maps  = []
    files = []
    sMes  = "0"
    if not os.path.isdir(_dirName):
        sMes = f'Нет каталога по указанному пути "{_dirName}"'
        return maps, files ,sMes 
    lstdir  = os.listdir(_dirName) 
    for l in lstdir:
        if l.lower().find(mapname) == 0 and l[-4::].lower() == 'html':
            maps.append(l[:-5])
            files.append(l)
    return maps, files ,sMes 

def setPointDir(startDir,data):
    #Сохранение точки пользователя
    if len(data) == 0 :
        sMes = f'Пустые параметры "{data}"'
        return 1, sMes
    mapname = data["mapname"]
    if startDir == None or mapname == None :
        sMes = f'Пустые параметры директория "{startDir}" карта "{mapname}"'
        return 1, sMes
    if not os.path.isdir(startDir):
        sMes = f'Нет каталога по указанному пути "{startDir}"'
        return 1, sMes

    try:    
        dirname = os.path.join(startDir,"GPX"+mapname[len(_MAPPREFIX):])
        if not os.path.exists(dirname):
            os.mkdir(dirname)

        filepoint = os.path.join(dirname,_FILEPOINTGPX)
        if os.path.exists(filepoint):
            with open(filepoint, 'r', encoding = 'UTF-8') as openfile:
                _data=openfile.read()   
                userpoints = eval(_data)
        else:
            userpoints ={}

        keymap = data["poitname"]
        userpoints[keymap] = {"g_lat":data["g_lat"] ,"g_lon":data["g_lon"], "mapname": data["mapname"]}

        with open(filepoint, 'w', encoding = 'UTF-8' ) as savefile:
            savefile.write(str(userpoints).replace('\'','"'))    
        return 0, f"Точка пользователя {data['poitname']} сохранена в {filepoint}"
    except Exception as e:
        return 1, traceback.format_exc()

def getPointDir(dirname):
    result = {}
    filepoint = os.path.join(dirname,_FILEPOINTGPX)
    if os.path.exists(filepoint):
        with open(filepoint, 'r', encoding = 'UTF-8') as openfile: 
            _data=openfile.read()   
            result = eval(_data)  
        result["Очистить точку"] = {"g_lat":"" ,"g_lon":"", "mapname":""}
    return result         

def getPointNameDir(dirname):
    result = []
    _result = getPointDir(dirname)
    for key in _result:
        result.append(key)
    return result 

def getPointCoordFromNameDir(dirname, pointname):
    result = {}
    _result = getPointDir(dirname)
    for key in _result:
        if key == pointname:
            result = _result[key]
    return result    

def autoUdateSetting(dirname, version):
    cssfile = "colors.css"
    rowFirst=f"/*{version}"
    textcolorinwhite = ["blue","darkblue","green","darkgreen","red","darkred","purple","black"]
    cssfilepath = os.path.join(dirname,cssfile)
    isNeedUpdate = True 
    if os.path.exists(cssfilepath):
        with open(cssfilepath, 'r', encoding = 'UTF-8') as openfile: 
            for line in openfile:
                isNeedUpdate = (line != rowFirst+"\n")
                break
    if isNeedUpdate:
        print(f"Обновление файла стилей {cssfilepath} для версии {version}")
        with open(cssfilepath, 'w', encoding = 'UTF-8' ) as savefile:
            savefile.write(rowFirst+"\n")    
            savefile.write("Файл стилей для справочников выбора цвета трека\n")
            savefile.write("Сформирован автоматически по списку цветов для текущей версии\n")
            savefile.write("*/\n")
            for _color in colorlist():
                savefile.write(f".l_{_color}"+"{\n")    
                savefile.write(f"background-color: {_color};\n") 
                if _color in textcolorinwhite:
                    savefile.write("color: white;\n") 
                savefile.write("}\n") 