""" 
    teleBot
    version="1.0.0"
    Модуль для методов работы с ботом телеграмма visiomapWebBot

"""

from aiogram import Bot
import os

async def send_info(filelist:list, infotxt:str, logfnc):
    API_TOKEN = os.getenv("BOTKEY", "0")

    if API_TOKEN == "0":
        logfnc("Неопределен токен телеграмм бота","error")
        return 
    
    bot = Bot(token=API_TOKEN)
    users = os.getenv("TGUSERS").split(',')

    if len(users) == 0:
        logfnc("Нет списка пользователей для оповещения бота")
        return 

    if len(filelist) == 0:
        logfnc("Нет списка файлов для отправки ботом ","error")
        return 

    for user in users:
        logfnc(f"Оповещение телеграмм ботом пользователя {user}")
        try:
            await bot.send_message(chat_id=user, text=infotxt)
        except Exception as e:
            if str(e) == "Chat not found":
                logfnc(f"Пользователь не подписан на рассылку")
            else:    
                logfnc(f"Ошибка оповещения {str(e)}","error")
            return 
            
        for _file in filelist:
            logfnc(f"Отправка файла {_file}")
            with open(_file, 'rb') as file:
                await bot.send_document(chat_id=user, document=file)