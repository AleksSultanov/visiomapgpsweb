function getXmlHttp() {
    // Создаём объект XMLHTTP
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function mpost(self,url,data,res_function, prm){
    /*
    Общая функция для отправки запроса
    self - объект
    url  - ссылка для запроса
    data - отправляемые данные
    res_function - функция для обработки ответа
    prm - параметр функции
    */
    var xmlhttp = getXmlHttp(); 
    xmlhttp.open('POST', url, true); // Открываем асинхронное соединение
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); // Отправляем кодировку
    xmlhttp.send(data); // Отправляем POST-запрос
    xmlhttp.onreadystatechange = function() { // вызов своей функции для обработки ответа
        res_function(xmlhttp,self,prm);
     };
}


function updateUserPoint(xmlhttp,self,prm){
    // Получение и установка значений из справочника Точки пользователя
    if (xmlhttp.readyState == 4) { // Ответ пришёл
        if(xmlhttp.status == 200) { // Сервер вернул код 200 (что хорошо)
            console.log(`xmlhttp.responseText ${xmlhttp.responseText}`);
            var data = JSON.parse(xmlhttp.responseText);
            if (data["res"] ==0) {
                id = prm["id"].replace('userPoint_','');
                document.getElementById("latitude_"+id).value = data["g_lat"];
                document.getElementById("longitude_"+id).value = data["g_lon"];
            } else console.log("Ошибка определения координат "+xmlhttp.responseText);


        }
    }
}

function sendUserPoint(element){
    // Отправка названия точки пользователя, для установки координатов
    // var formData = new FormData(element);
    var data="name="+encodeURIComponent(element.value)+"&"+"dirname="+encodeURIComponent(location.pathname);
    var prm  = {id : element.id};
    mpost(element,"/referuserpoint",data,updateUserPoint,prm);
}


function sendDescription(element,lat,lon,descriptionId){
    // Определение названия места по координатам
    var data="g_lat="+encodeURIComponent(lat)+"&"+"g_lon="+encodeURIComponent(lon);
    var prm  = {id : descriptionId};
    mpost(element,"/getplace",data,updateDescription,prm);
}

function updateDescription(xmlhttp,self,prm){
    // Получение и установка значений места по координатам
    if (xmlhttp.readyState == 4) { // Ответ пришёл
        if(xmlhttp.status == 200) { // Сервер вернул код 200 (что хорошо)
            // console.log(`xmlhttp.responseText ${xmlhttp.responseText}`);
            var data = JSON.parse(xmlhttp.responseText);
            if (data["res_code"] ==200) {
                var id = prm["id"];
                var poitname = data["poitname"]; 
                document.getElementById("description_"+id).value = poitname;
            } else console.log("Ошибка определения координат "+xmlhttp.responseText);


        }
    }
}


function updateIcon(xmlhttp,self,prm){
    // Обновление иконки трека
    if (xmlhttp.readyState == 4) { // Ответ пришёл
        if(xmlhttp.status == 200) { // Сервер вернул код 200 (что хорошо)
            var data = JSON.parse(xmlhttp.responseText);
            if (data["res"] ==0) {
                id = prm["id"].replace('_type','_icon');
                var class_name = data["class_name"]; 
                document.getElementById(id).className = "fa "+class_name+" fa-fw"; 
            } else console.log("Ошибка обновления иконки "+xmlhttp.responseText);


        }
    }
}

function sendupdtype(element){
    // Запрос на обновления иконки по типу
    var idx = element.options.selectedIndex;
    var value = element.options[idx].value;
    var data  = "type="+value;
    var prm  = {id : element.id};
    mpost(element,"/typeicon",data,updateIcon,prm);
}
