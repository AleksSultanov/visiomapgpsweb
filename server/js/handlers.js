// Увеличения картинки, для события onclick
function handlerImgZoomClik(element) {
    if (element.width == 300) {
        element.style.width="600px";
        return;
    } 

    if (element.width == 600) {
        element.style.width="300px";
        return;
    }    
}

// Показ панели с кнопками сохранения
function setvisiblesavefoto(form) {
    document.getElementById("savesettingboxid").style.display = "block";
}

// Отмена изменений на форме и скрытие панели с кнопками сохранения
function resetchangefoto(form) {
    document.getElementById("savesettingboxid").style.display = "none";
    form.reset();
}


// Проверка обязательности ввода для события onchange
function handlerEditChange(element,caption) {
    if (element.value == "") {
        element.placeholder="Заполните поле ("+caption+")";
        element.required="True";
        alert("Заполните поле "+ caption);
    }
}

// Сообщения для нажания на кнопку формирования карты
function presubmitMap(form, pageid) {
    handlerPageSelect(pageid, "w")
    form.style.display = "none";
    document.getElementById("infoload").innerHTML = "Подождите. Формирование карты ...";
    form.submit();
}

// Проверка на расширение файла - check (список расширений через ,)
function handlerValidExt(element,check){
    var filename = element.files[0]["name"];
    var arrayfilename = filename.match(/(.+\.)([A-Za-z0-9]{3})$/);
    if (!arrayfilename)
        arrayfilename = filename.match(/(.+\.)([A-Za-z0-9]{4})$/);
    var ext = arrayfilename[2].toLowerCase();
    if (! check.split(',').includes(ext)){
        element.value = "";
        document.getElementById("filename").innerHTML = "";
        alert("Ошибка, файл должен быть "+check+" формата, текущий формат: " + ext);
        return 0;
    } 
    else {
       document.getElementById("filename").innerHTML = filename;
       return 1;
    }
}
//Обновление выбранного цвета
function handlerColorChange(element){
    var idx = element.options.selectedIndex;
    var value = element.options[idx].value;
    element.className = "flinecolor l_"+value;  
}

//Обновление выбранного цвета иконки
function handlerIconColorChange(element){
    var idx = element.options.selectedIndex;
    var value = element.options[idx].value;
    var id = element.id.replace('_iconcolor','_icon');
    element.className = "ficoncolor l_"+value;  
    document.getElementById(id).style="color:"+value; 
}


//Меняет класс блоков в зависимости от формата- градусы или десятичный формат
function setvisDivDmDec(mode){
    var divsDEG = document.getElementsByName("blockDEG");
    var divsDM = document.getElementsByName("blockDM");
    if (mode !='DEG'){
        for (var i = 0; i < divsDM.length; i++) {
            divsDM[i].className = 'blockDM';
        }
        for (var i = 0; i < divsDEG.length; i++) { 
            divsDEG[i].className = 'blockDEGH';            
        }    
    }else {

        for (var i = 0; i < divsDM.length; i++) {
            divsDM[i].className = 'blockDMH';
        }    
        for (var i = 0; i < divsDEG.length; i++) { 
            divsDEG[i].className = 'blockDEG';            
        }    
    }  
}

//Переключение градусы, десятичный формат
function handlerchangeDmDec(element){
    if (element.value !='DEG'){
        document.getElementById("latitude").value = "";
        document.getElementById("longitude").value = "";
        document.getElementById("latitude").required = false;
        document.getElementById("longitude").required = false;
        document.getElementById("latitudeDeg").required = true;
        document.getElementById("latitudeMin").required = true;
        document.getElementById("longitudeDeg").required = true;
        document.getElementById("longitudeMin").required = true;
    }else {
        document.getElementById("latitudeDeg").value = "";
        document.getElementById("latitudeMin").value = "";
        document.getElementById("latitudeSec").value = "";
        document.getElementById("longitudeDeg").value = "";
        document.getElementById("longitudeMin").value = "";
        document.getElementById("longitudeSec").value = "";
        document.getElementById("latitude").required = true;
        document.getElementById("longitude").required = true;
        document.getElementById("latitudeDeg").required = false;
        document.getElementById("latitudeMin").required = false;
        document.getElementById("longitudeDeg").required = false;
        document.getElementById("longitudeMin").required = false;

    }  
    setvisDivDmDec(element.value);
}
/*Проверка на числовое значение */
function handlerValidFloat(element){
    var floattxt = element.value.match(/^[\-\d]?\d+[\.\,]?\d*/);
    if (floattxt != null && floattxt.length >0){
        element.value = floattxt[0];
    }else {
        alert("Неверное значение "+ element.value+ ".Должно быть число");
        element.value ="";

    }
}    

/*Установка флага текущей страницы*/
function handlerPageSelect(pageid, flag="s"){
    var elements = document.getElementsByClassName('lbround');
    for (var i = 0; i < elements.length; i++){
        if (elements[i].id == pageid){
            if (flag == "s") elements[i].className='lbround lbroundSel';
            if (flag == "w") elements[i].className='lbround lbroundWork';
            if (flag == "e") elements[i].className='lbround lbroundErr';
        }
        else {
            elements[i].className='lbround';
        }
    }
}

// Сообщения для нажания на кнопку загрузки трека для редактирования
function presubmitTrackEdit(form, pageid) {
    handlerPageSelect(pageid, "w")
    form.style.display = "none";
    document.getElementById("infoload").innerHTML = "Подождите. Парсим файл ...";
    form.submit();
}

// Функция для запуска отправки формы, после загрузки файла и его проверки на расширение
function loadchksubmit(element, extcheck, formid, pageid){
    if (handlerValidExt(element,extcheck) == 1 ) {
        form = document.getElementById(formid);
        presubmitTrackEdit(form, pageid);

    }
}
