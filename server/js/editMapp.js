/**
 * Скрипт для создания карты
 * Версия 1.0
  */
var visiomap;
var editmap;
var Dataj = {};
var track_obj = {polylines:{},
                circlemarkers:{},
                featureGroups:{},
                tracksname:{}
                }
var defZoom = 3;
var defLat = 49;
var defLon = 69;
var defColor = "blue";
var defFillColor = "lightblue";
var iscell = false;


/* Создание карты с меткой*/
function visiomapadd(lat,lon,mapId){
    var startlat = defLat;
    var startlon = defLon;
    var zoom_ = defZoom;
    var isEmpty = true;
    if (lat !== 0 && lon !== 0){
        startlat = lat;
        startlon = lon;
        zoom_    = 12;
        isEmpty  = false;
    }

    if (visiomap) {
        visiomap.invalidateSize();
        visiomap.off();
        visiomap.remove();

    }
    // Создаю карту
    visiomap = L.map(mapId,
        {
            center: [startlat, startlon],
            crs: L.CRS.EPSG3857,
            zoom: zoom_,
            zoomControl: true,
            preferCanvas: false,
        }
    );
    // Добавляем слой
    var visiomaplayer = L.tileLayer(
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {"attribution": "Data by \u0026copy; \u003ca href=\"http://openstreetmap.org\"\u003eOpenStreetMap\u003c/a\u003e, under \u003ca href=\"http://www.openstreetmap.org/copyright\"\u003eODbL\u003c/a\u003e.", "detectRetina": false, "maxNativeZoom": 18, "maxZoom": 18, "minZoom": 0, "noWrap": false, "opacity": 1, "subdomains": "abc", "tms": false}
    ).addTo(visiomap);
    if (!isEmpty){
        // Добавляю метку
        var marker_ = L.marker([lat, lon],{}).addTo(visiomap);
        var icon_ = getdeficon();
        marker_.setIcon(icon_);
        var popup_ = L.popup({"maxWidth": "100%"});
        var urls =  mapsUrl(lat,lon); 
        var blockurl = '<b>На картах:</b><br><nobr>'+urls+'</nobr>';
        var html_ = $('<div id="html_1" style="width: 100.0%; height: 100.0%;">'+blockurl+'</div>')[0];
        popup_.setContent(html_);
        marker_.bindPopup(popup_);
    }


}

/*Получение атрибутов точки трека*/
function datapoint(track,key){
    return Dataj[track]["points"][key];
}

/*Получение точек трека*/
function trackPoints(track){
    return Dataj[track]["points"];
}



/**HTML Блок для меню */
function getHtmlPopup(track,key){
    var lat_ = datapoint(track,key)["lat"];
    var lon_ = datapoint(track,key)["lon"];
    var trname = Dataj[track]["name"];
    var blocInfo = '<h4>'+trname+'</h4>';
    blocInfo += '<b>№</b> '+key+'<br>';
    blocInfo += '<nobr>('+lat_+','+lon_+')</nobr><br>';
    blocInfo += datapoint(track,key)["ele"]+' м<br>';
    blocInfo += '<nobr>'+datapoint(track,key)["time"]+'</nobr><br>';

    var urls =  mapsUrl(lat_,lon_); 
    var blockurl = '<hr> <b>На картах:</b><br><nobr>'+urls+'</nobr>';


    var blocAction = '<hr> <label class="pointAction" onclick="pointDelInterval(\''+track+'\',\''+key+'\')">Выделить и удалить</label>';
    blocAction += '<br> <label class="pointAction" onclick="pointDelStart(\''+track+'\',\''+key+'\')"><nobr>Удалить все сначала до текущей </nobr></label>'; 
    blocAction += '<br> <label class="pointAction" onclick="pointDelEnd(\''+track+'\',\''+key+'\')">Удалить до конца</label>'; 
    blocAction += '<br> <label class="pointAction" onclick="pointDel(\''+track+'\',\''+key+'\')">Удалить точку</label>';
    if (datapoint(track,key)["mod"] == "s") {
        blocAction = '<hr> <label class="pointAction" onclick="pointNorm(\''+track+'\',\''+key+'\')">Отменить выделение точки </label>';
    }    
    if (datapoint(track,key)["mod"] == "d") {
        blocAction = '<hr> <label class="pointAction" onclick="pointNorm(\''+track+'\',\''+key+'\')">Отменить удаление точки</label>';
    }    
    var html_ = $('<div id="html_1" style="width: 100.0%; height: 100.0%;">'+blocInfo+blockurl+blocAction+'</div>')[0];
    return html_;
}

/**Точка для редактора трека */
function getcircleMarker(track,key){
    var lat_ = datapoint(track,key)["lat"];
    var lon_ = datapoint(track,key)["lon"];
    var popup_ = L.popup({"maxWidth": "100%"});
    
    var color_ = defColor;
    var fillColor_ = defFillColor;
    if (datapoint(track,key)["mod"] == "s") {
        color_ = "red";
        fillColor_ = "reg";
    }    
    if (datapoint(track,key)["mod"] == "d") {
        color_ = "gray";
        fillColor_ = "gray";
    }
    var circle_marker = L.circleMarker([lat_, lon_],
        {"bubblingMouseEvents": true, 
        "color": color_, "dashArray": null, "dashOffset": null,
         "fill": true, "fillColor": fillColor_, "fillOpacity": 0.2,
         "fillRule": "evenodd", "lineCap": "round", "lineJoin": "round",
         "opacity": 0.5, "radius": 5, "stroke": true, "weight": 3}
    );
    var html_ = getHtmlPopup(track,key);
    popup_.setContent(html_);
    circle_marker.bindPopup(popup_);
    return circle_marker;
}    

/**Формирование слоя для трека */
function creTrackGroup(track){
    var lines = [];
    var i = 0;
    var fg = L.featureGroup({}).addTo(editmap); 
    for (key in trackPoints(track)){
        //Добавляем точки
        var lat_ = datapoint(track,key)["lat"];
        var lon_ = datapoint(track,key)["lon"];
        var circle_marker = getcircleMarker(track,key).addTo(fg);
        track_obj.circlemarkers[track][key] = circle_marker;
        if (datapoint(track,key)["mod"] != "d") {
            lines[i] = [lat_, lon_];
            i+=1;
        }    
    }
    //добавляю линию
    var poly_line = getPolyline(lines).addTo(fg);
    track_obj.polylines[track] = poly_line;
    //сохраняю слой для трека
    track_obj.featureGroups[track] = fg; 
}


/**Рисует трек не для удаленных точек */
function getPolyline(lines){
    var poly_line = L.polyline(lines,
        {"bubblingMouseEvents": true, 
        "color": defColor, 
        "dashArray": null, "dashOffset": null, "fill": false, 
        "fillColor": defFillColor, "fillOpacity": 0.2, "fillRule": "evenodd",
        "lineCap": "round", "lineJoin": "round",
        "noClip": false, "opacity": 0.5, "smoothFactor": 1.0, 
        "stroke": true, "weight": "2"}
    );

    return poly_line;
}    

function getPolylineTr(track){
    var lines = [];
    var i = 0;
    for (key in trackPoints(track)) {
        if (datapoint(track,key)["mod"] != "d") {
            var lat_ = datapoint(track,key)["lat"];
            var lon_ = datapoint(track,key)["lon"];
            lines[i] = [lat_, lon_];
            i+=1;
        }
    }
    return getPolyline(lines);
}   


/**Перересовываю линию трека */
function reloadTracLine(track){
    track_obj.polylines[track].remove();
    var fg = track_obj.featureGroups[track];  
    var polyline = getPolylineTr(track).addTo(fg);
    track_obj.polylines[track] = polyline;
}


/* Создание карты для редактирования трека*/
function editmapNew(mapId,dataId,tracksId){
    var Data = document.getElementById(dataId).innerHTML;
    var Tracks = document.getElementById(tracksId).innerHTML;
    if (Data) {
        Dataj = JSON.parse(Data);
    }
    track_obj.tracksname = Tracks;

    for (track in Dataj){
        track_obj.circlemarkers[track] = [];
        for (key in trackPoints(track)){
            datapoint(track,key)["mod"] = "n"
        }
    }
    editmapadd(mapId)
}

/* Создание карты для редактирования трека*/
function editmapadd(mapId){
    var startlat = defLat;
    var startlon = defLon;
    var zoom_ = defZoom;
    for (track in Dataj){
        startlat = datapoint(track,"1")["lat"];
        startlon = datapoint(track,"1")["lon"];
        zoom_ = 15;
    }

    if (editmap) {   
        // Удаляю карту
        editmap.invalidateSize();
        editmap.off();
        editmap.remove();
    }     
    // Создаю карту
    editmap = L.map(mapId,
        {
            center: [startlat, startlon],
            crs: L.CRS.EPSG3857,
            zoom: zoom_,
            zoomControl: true,
            preferCanvas: false,
        }
    );
    
    // Добавляем слой
    var tile_layer_OSM = L.tileLayer(
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {"attribution": "Data by \u0026copy; \u003ca href=\"http://openstreetmap.org\"\u003eOpenStreetMap\u003c/a\u003e,under \u003ca href=\"http://www.openstreetmap.org/copyright\"\u003eODbL\u003c/a\u003e.",
            "detectRetina": false, "maxNativeZoom": 99, "maxZoom": 99, "minZoom": 0, "noWrap": false, "opacity": 1, "subdomains": "abc", "tms": false}
    ).addTo(editmap);

    var tile_layer_google = L.tileLayer(
        "http://{s}.google.com/vt/lyrs=s\u0026x={x}\u0026y={y}\u0026z={z}",
        {"attribution": "google", "detectRetina": false, "maxNativeZoom": 99, "maxZoom": 99, "minZoom": 0, "noWrap": false, "opacity": 1,
            "subdomains": ["mt0", "mt1", "mt2", "mt3"], "tms": false}
    ).addTo(editmap);


    var tile_layer_carto = L.tileLayer(
        "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
        {"attribution": "\u0026copy; \u003ca href=\"http://www.openstreetmap.org/copyright\"\u003eOpenStreetMap\u003c/a\u003e contributors \u0026copy; \u003ca href=\"http://cartodb.com/attributions\"\u003eCartoDB\u003c/a\u003e, CartoDB \u003ca href =\"http://cartodb.com/attributions\"\u003eattributions\u003c/a\u003e",
            "detectRetina": false, "maxNativeZoom": 99, "maxZoom": 99, "minZoom": 0, "noWrap": false, "opacity": 1, "tms": false}
    ).addTo(editmap);
    
    for (track in Dataj){
        /**Формирование слоя для трека */
        creTrackGroup(track);
    }  
    //Панель управления
    var layer_control = {
        base_layers : {
            "OpenStreet Map" : tile_layer_OSM,
            "Google Maps" : tile_layer_google,
            "Cartodb Positron" : tile_layer_carto,
        },
    };
    L.control.layers(
        layer_control.base_layers,
        {},
        {"autoZIndex": true, "collapsed": true, "position": "topright"}
    ).addTo(editmap);  
    tile_layer_google.remove();
    tile_layer_carto.remove();
    updateActionInfo();
}


function updateActionInfo(){
    var cntD = 0;
    var cnt = 0;
    for (track in Dataj){
        for (key in trackPoints(track)){
            cnt +=1;
            if (datapoint(track,key)["mod"] == "d") {
                cntD +=1;
            }    
        }
        var trackid_del = track+"_del";
        var trackid_all = track+"_all";
        document.getElementById(trackid_all).innerHTML=cnt;
        document.getElementById(trackid_del).innerHTML=cntD;
        cnt = 0
        cntD = 0
    
    }    
}        


/*Перерисовывает круговую точку на карте*/
function circleReload(track,key,udate=true){
    track_obj.circlemarkers[track][key].remove();
    var circle_marker = getcircleMarker(track,key).addTo(track_obj.featureGroups[track]);
    track_obj.circlemarkers[track][key] = circle_marker;
    if (udate){
        updateActionInfo();
    } 
    
}

/*Отмечает точку на треке*/
function pointSel(track,key){
    iscell = true;
    datapoint(track,key)["mod"] = "s";
    circleReload(track,key);
}

/*Отменяет выделение точки*/
function pointNorm(track,key){
    if (datapoint(track,key)["mod"] == "s"){
        iscell = false;
    }
    datapoint(track,key)["mod"] = "n";
    circleReload(track,key);
    reloadTracLine(track);
}

/*Помечает как удаленную*/
function pointDel(track,key){
    datapoint(track,key)["mod"] = "d";
    circleReload(track,key);
    reloadTracLine(track);
}

/*Помечает как удаленные выделенный интервал (между mod == "s")*/
function pointDelSel(){
    var Statr = false;
    var StatrPre = false;
    for (track in Dataj){
        for (key in trackPoints(track)){
            var mod = datapoint(track,key)["mod"];
            if (mod == "s"){
                Statr = !Statr; 
            }
            if ((Statr||StatrPre) & (mod == "s" || mod == "n") ){
                datapoint(track,key)["mod"] = "d";
                circleReload(track,key,false);
            }
            StatrPre = Statr;
        }
        reloadTracLine(track);
    }
    updateActionInfo();
    iscell = false;
}    

/*Помечает как удаленные выделенный интервал (между mod == "s")*/
function pointDelSel2(track,skey){
    var Statr = false;
    var StatrPre = false;
    for (key in trackPoints(track)){
        var mod = datapoint(track,key)["mod"];
        if (mod == "s" || key == skey){
            Statr = !Statr; 
        }
        if ((Statr||StatrPre) & (mod == "s" || mod == "n"|| key == skey) ){
            datapoint(track,key)["mod"] = "d";
            circleReload(track,key,false);
        }
        StatrPre = Statr;
    }
    reloadTracLine(track);
    updateActionInfo();
    iscell = false;
}    

/*Помечает на выделение если первая точка или помечает как удаленные выделенный интервал (между mod == "s")*/
function pointDelInterval(track,key){
    if (! iscell ){
        pointSel(track,key);
    }else {
        pointDelSel2(track,key);
    }
}

/*Помечает как удаленные до конца начиная с текущей точки skey*/
function pointDelEnd(track,skey){
    var Statr = false;
    for (_key in trackPoints(track)){
        if (_key == skey){
            Statr = true 
        }
        if (Statr ){
            datapoint(track,_key)["mod"] = "d";
            circleReload(track,_key,false);
        }
    }
    reloadTracLine(track);
    updateActionInfo();
}    

/*Помечает как удаленные с начала трека до текущей точки skey*/
function pointDelStart(track,skey){
    for (_key in trackPoints(track)){
        datapoint(track,_key)["mod"] = "d";
        circleReload(track,_key,false);
        if (_key == skey){
            break
        }
    }
    reloadTracLine(track);
    updateActionInfo();
}    


/*Включает, выключает видимость трека*/ 
function setvistrack(trackId,visible){
    if (visible) {
        creTrackGroup(trackId);
        Dataj[track]["visible"] = "1";
    }else {
        track_obj.featureGroups[trackId].remove();
        Dataj[track]["visible"] = "0";

    }
}


/*Включает, выключает видимость трека по чекбоксу*/ 
function setvistrackch(trackId,element){
    setvistrack(trackId,element.checked);
}


/*Экспорт текущего трека. Функция успешного ответа на экспорт*/ 
function successExpfile(data,d){
    var list = $("<ul>");
    $("#ExpTrackcontent").empty();
    if (data['res_code'] == "0" ){
        for (f in data['files']) {
            $('<li>'+data['files'][f]+'</li>').appendTo(list);
        }
        $("#ExpTrackcontent").append(list);
        $("#export").text("Результат экспорта");
    } else {
        $('<li>'+data['error']+'</li>').appendTo(list);
        $("#ExpTrackcontent").append(list);
        $("#export").empty();
        $("#exportErr").text("Ошибка экспорта");
    }
    
    
}

/*Экспорт текущего трека. Функция для формирования текста до отправки на экспорт*/ 
function beforeSendExpfile(){
    $("#export").text("Результат экспорта");
    $("#exportErr").empty();
    $("#ExpTrackcontent").text("Формирование файлов ...");
}

/*Экспорт текущего трека. Функция ошибочного ответа на экспорт*/ 
function errorSendExpfile(response){
    $("#exportErr").text("Ошибка экспорта");
    $("#export").empty();
    $("#ExpTrackcontent").text(response);
}

/*Экспорт текущего трека*/ 
function trackExp(){
    for (_track in Dataj){
        var trId = _track+'_name';
        var newname = document.getElementById(trId).value;
        Dataj[_track]["newname"] = newname;
    }  
    $.ajax({
        url: '/exptrack',
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({tracksdir:Dataj,filename: $("#filenameid").val()}),
        beforeSend: beforeSendExpfile,
        success: successExpfile,
        error : errorSendExpfile
    });
}
