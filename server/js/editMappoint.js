/**
 * Скрипт для создания элементов на карте
 * Версия 3.0
 * Добавляет обработку событий по клику мыши
 * ctrl+click - Создание точки
  */
var cntpoint  = 0;
var isadd = false;
function start(){
    /*Добавление событий и блоков*/
    if (!isadd){
        $(document).ready(function() {
            $("body").bind("click",customClick);
        });  
        var divInfo = $("<div>"); 
        $('<span id="editinfoTxt">Нажмите cntrl+левый клик мыши, чтобы создать точку пользователя</span>').appendTo(divInfo);
        $('<span id="reloadinfo"></span>').appendTo(divInfo);
        $("body").prepend(divInfo);
        isadd = true;
    }   
}

start();//Запуск внедрения логики скрипта

function isEdit(){
    /*Если в строке параметров есть mapedit - режим редактирования*/
    return location.search.indexOf("mapedit") >= 0;
}


function mapsUrl(g_lat, g_lon){
    /*Блок с ссылками на ресурсы с картами*/
    var C_TARGET = 'target = "_blank"';
    var C_ICON_W = 'width="20"';
    var C_SCALE = 16;
    var urlGoogle  = '<a href = "http://www.google.com/maps?q='+g_lat+','+g_lon+'" '+C_TARGET+
        '><img src="http://www.google.com/favicon.ico"'+C_ICON_W+' ></a>';
    var urlOstreet = '<a href = "https://www.openstreetmap.org/#map='+C_SCALE+'/'+g_lat+'/'+g_lon+'" '+C_TARGET+
        '><img src="https://openstreetmap.org/favicon.ico" '+C_ICON_W+' ></a>';
    var urlOSMAND  = '<a href = "https://osmand.net/go?lat='+g_lat+'&lon='+g_lon+'&z='+C_SCALE+'" '+C_TARGET+
        '><img src="https://osmand.net/images/favicons/favicon.ico" '+C_ICON_W+' ></a>';
    var urlYandex  = '<a href = "https://yandex.ru/maps?pt='+g_lon+','+g_lat+'&z='+C_SCALE+'" '+C_TARGET+
        '><img src="https://yandex.ru/maps/favicon.ico" '+C_ICON_W+' ></a>';
    var urlYandexW  = '<a href = "https://yandex.ru/pogoda/maps/nowcast?lat='+g_lat+'&lon='+g_lon+'"'+C_TARGET+
        '><img src="https://yandex.ru/pogoda/favicon.ico" '+C_ICON_W+' ></a>';
    var urlWindy   = '<a href = "https://windy.com/'+g_lat+'/'+g_lon+'&z='+C_SCALE+'" '+C_TARGET+'><img src="https://windy.com/favicon.ico" '+C_ICON_W+' ></a>';
   
    var url =urlGoogle+urlOstreet+urlOSMAND+urlYandex+urlYandexW+urlWindy;
    return decodeURI(url);
}

function customClick(event){
    /*Переключатель событий по комбинациям*/
    if (event.ctrlKey) pointAdd();
}

/*Иконка для точки */
function getdeficon(){
    var icon_ = L.AwesomeMarkers.icon({"extraClasses": "fa-rotate-0", "icon": "info", "iconColor": "black", "markerColor": "darkred", "prefix": "fa"});
    return icon_;
}

function pointAdd() {
    /*Добавление пользовательской точки*/
    var nodelist = document.getElementsByClassName("leaflet-control-mouseposition leaflet-control");
    var divmap = document.getElementsByClassName("folium-map");
    var coordstxt = nodelist[0].innerHTML; 
    var coords = coordstxt.split(":");
    
    if (confirm("Сохранить точку: "+coordstxt+" ?")) {
        var uspointname = prompt("Задайте название для пользовательской точки");

        var marker_ = L.marker([coords[0], coords[1]],{}).addTo(eval(divmap[0].id));
        var icon_ = getdeficon();
        marker_.setIcon(icon_);
        var popup_ = L.popup({"maxWidth": "100%"});
        var urls =  mapsUrl(coords[0].trim(),coords[1].trim()); 
        var blockurl = '<hr><b>На картах:</b><br><nobr>'+urls+'</nobr>';
        var html_ = $('<div id="html_1" style="width: 100.0%; height: 100.0%;"><nobr><font color="purple"><b>Метка пользователя</b></font></nobr><br>'+uspointname+blockurl+'</div>')[0];
        popup_.setContent(html_);
        marker_.bindPopup(popup_);

        if (isEdit() || true ){ 
        // if (isEdit()){             
            var userpoint = {g_lat:coords[0], g_lon:coords[1],poitname:uspointname,mapname:location.pathname};
            $.post('/addpoint',userpoint,function(data){
                var errtxt = data["res_msg"] ;
                var errcode = data["res_code"] ;
                if (errcode == "0") {
                    cntpoint++;
                    document.getElementById("reloadinfo").innerHTML = ". Установлено точек: "+cntpoint+'. Нажмите <a href="/visiomap">переформировать</a> и сохраните их на новой карте';
                } else document.getElementById("reloadinfo").innerHTML = '.<font color="red"> Ошибка сохранения точки: '+decodeURI(errtxt)+'</font>';
            });
        }             
    }    
}