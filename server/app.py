""" Визуализация gps треков на карте

#### Возможности:

* Визуализация треков в формате gpx, kml.
* Расположение ссылок на фотографии.

#### Описание:

Для работы необходимо в текущей папке наличие папок fotoYYY и gpxYYY <br>
    , где YYY - произвольное имя - имя карты. <br>
Каталог fotoYYY (необязателен) - содержит фотографии для карты, которые будут загружены на ЯндекскДиск 
    и отображены на карте<br>
Каталог gpxYYY - содержит треки в формате GPX для карты<br>

Для каждого YYY создается своя карта в каталоге map, имя карты mapYYY.html 
"""

import os

import traceback
from fastapi import FastAPI, Request, File
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
import json

from typing import Union

from mapLib.setformw import SetForm
import mapLib.finddir as finddir
import mapLib.maplog as _mlog
import mapLib.mapcfg as attr
import mapLib.reqGeoData as geodata
import mapLib.teleBot as tgbot
import mapLib.mapparse as mparse

import server.mapCre as mc
import server.mapDemo as md

_title = "VisioMapWeb"
_version ="2.5.0"
_appication = f"{_title} {_version}"
_description = ""
_openapi_tags = [
    {
        "name": "index",
        "description": "Главная",
    },
    {
        "name": "visiomap",
        "description": "Создание карты",
    },
    {
        "name": "kmlhtml",
        "description": "Импорт адресов из kml",
    },
    {
        "name": "result",
        "description": "Результаты",
    },
    {
        "name": "docs",
        "description": "Документация",
    },
    {
        "name": "map",
        "description": "Карта",
    },
    {
        "name": "adr",
        "description": "Адреса",
    },
    {
        "name": "editfoto",
        "description": "Просмотр или установка координат фотографий",
    },
    { 
        "name": "savefoto",
        "description": "Сохранение страницы с фотографиями",
    },
    { 
        "name": "reference",
        "description": "Обработка справочников",
    },
    { 
        "name": "getplace",
        "description": "Получение названия места по координатам",
    },
    { 
        "name": "find",
        "description": "Поиск по координатам",
    },
    { 
        "name": "trackEdit",
        "description": "Редактор треков",
    },
    { 
        "name": "expTrack",
        "description": "Экспорт треков",
    },

]

# Доступ СORS (Cross-Origin Resource Sharing)
origins = [
    "http://localhost",
    "http://localhost:8000",
]

app = FastAPI(title=_title, description = _description, version = _version,  openapi_tags = _openapi_tags)
app.mount("/static", StaticFiles(directory="./server/static"), name="static")
app.mount("/staticExt", StaticFiles(directory="./server/staticExt"), name="staticExt")
app.mount("/js", StaticFiles(directory="./server/js"), name="js")
app.mount("/jsExt", StaticFiles(directory="./server/jsExt"), name="jsExt")
app.mount("/fonts", StaticFiles(directory="./server/fonts"), name="fonts")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

templates = Jinja2Templates(directory="./server/templates")
templatesRes = Jinja2Templates(directory=f"./{mc._MAPDIRSAVE}")

_START_DIR = os.getcwd() 

@app.get("/map{mapname}/", response_class=HTMLResponse, tags=['map'])
async def viewmap(request: Request, mapname: str):
#Показ страницы с картой    
    return templatesRes.TemplateResponse(f"{finddir._MAPPREFIX}{mapname}{mc._HTMLEXT}", {"request": request})    

@app.get("/adr{adresname}/", response_class=HTMLResponse, tags=['adr'])
#Показ страницы с адресами
async def viewadres(request: Request, adresname: str):
    return templatesRes.TemplateResponse(f"{mc._ADRPREFIX}{adresname}{mc._HTMLEXT}", {"request": request})    

@app.get("/", tags=['index'])
#Начальная страница
async def get_root(request: Request):
    _params = {"request": request, "nameTitle":_title, "version":_version}
    return templates.TemplateResponse("index.html",_params) 

@app.get("/visiomap", tags=['visiomap'])
#Создание карты
async def get_visiomap(request: Request):
    try:
        finddir.autoUdateSetting(dirname="./server/static",version=_version)
        sform = SetForm('SettingForm')
        fsettings = sform.values
        maplist = sform.mapLayerList
        filelist = finddir.getMapDirectory(_START_DIR)
        a  = attr.reedAttr()
        _params = {"request": request, "fsettings": fsettings, "maps":maplist, "filelst":filelist, "selectType":a, "startDir":_START_DIR, "colorlist":attr.colorlinelist() }
        return templates.TemplateResponse("visioMap.html",_params) 
    except Exception as e:
        _params = {"request": request, "errlist": str(e), "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)     

@app.post("/mapresult", tags=['result'])
#Результат
async def get_resb(request: Request, kmlfile:Union[bytes, None] = File(default=None)):
    _mlog._LOG = []
    _mlog._PRINT_STATUS_ERR = True 
    try:   
        htmmaplist = []
        filemaplist = []
        _mapsNew   = []
        _mapsDir   = []       
        maplist    = []
        _err       = "0"

        rform = await request.form()

        isCreMap  = (rform.get("checkedCreMap") != None)
        isDemo = (rform.get("checkedCreDemoIcon") != None)
        isDemoLine = (rform.get("checkedCreDemoLine") != None)
        isKml = (rform.get("btImpKml") != None)
        isDemoEmpty = (rform.get("checkedCreDemoEmpty") != None)
        isSendTg = (rform.get("sendtgW") != None)
        
        

        finddir.load_env()   
        sform = SetForm('SettingForm')
        sform.savevalues(rform)
        fsettings = sform.values
        md._MAPlAYER = sform.mapLayerList
        md._MAPDIRSAVE = mc._MAPDIRSAVE

        if isCreMap: 
            # Формируем карты
            a  = attr.reedAttr()
            _mapfilelist  = finddir.getMapDirectory(_START_DIR)
            # # Формирую только отмеченные карты
            mapfilelist = mc.mapSettinglst(_mapfilelist,rform)
            mc._MAPSCRIPTFLIE = "../js/editMappoint.js"
            _mapList, _err = mc.runCreMap(a,fsettings,mapfilelist)
            maplist += _mapList

        if isDemo :
            _mapList = md.creMapIcon()
            maplist += _mapList

        if isDemoLine :
            _mapList = md.creMapLine()
            maplist += _mapList
        
        if isDemoEmpty :    
            md._MAPSCRIPTFLIE = "../js/editMappoint.js"
            _mapList, _err = md.creMapEmpty()
            maplist += _mapList

        _addrsnew = []
        if isKml:
            _fileObj = rform.get("kmlfile")
            if _fileObj == None or _fileObj.filename == "":
                _err = "Не выбран файл для импорта"

            if _err == "0":
                _addrsnew, _err =  mc.runCreAdrKml(kmlfile,_fileObj.filename)

        if _err != "0":  
            _params = {"request": request, "errlist": _err,"logs":_mlog._LOG}
            return templates.TemplateResponse("mapresultErr.html",_params)     

        for _m in maplist:
            _row = {"mapname":_m["mapname"],
                    "mapfile":_m["mapfile"],
                    "countgpx":_m["countgpx"],
                    "countfoto":_m["countfoto"],
                    }
            _mapsNew.append(_m["mapname"])
            filemaplist.append(_m["mapfile"])        
            htmmaplist.append(_row) 

        #Поиск сформированных ранее карт 
        _maps,_files,_err = finddir.getMapListDirectory(mc._MAPDIRSAVE,finddir._MAPPREFIX) 
        if _err == "0":
            _mapsDir = list(filter(lambda _x:_x not in _mapsNew, _maps))
        #Поиск сформированных ранее адресов 
        _adrs,_files,_err = finddir.getMapListDirectory(mc._MAPDIRSAVE,mc._ADRPREFIX) 
        if _err == "0":
            _adrsDir = list(filter(lambda _x:_x not in _addrsnew, _adrs))
        #Отправка результата через телеграмм бот
        if isSendTg and len(filemaplist) > 0 :
            await tgbot.send_info(filemaplist,'Сформированы новые карты',_mlog._save)
        #Вывод результата
        _params = {"request": request, "htmmaplist": htmmaplist,"logs":_mlog._LOG, "mapsdir":_mapsDir, "addrsdir":_adrsDir, "addrsnew":_addrsnew, "dir":mc._MAPDIRSAVE}    
        return templates.TemplateResponse("mapresult.html",_params)
    except Exception as e:
        _params = {"request": request, "errlist": str(e),"logs":_mlog._LOG, "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)     


@app.get("/mapresult", tags=['result'])
#Результат
async def get_res(request: Request):
    _mlog._LOG = []
    _mlog._PRINT_STATUS_ERR = True 
    try:   
        _maps,_files,_err = finddir.getMapListDirectory(mc._MAPDIRSAVE,finddir._MAPPREFIX) 
        if _err != "0":
            return _err    
        _adrs,_files,_err = finddir.getMapListDirectory(mc._MAPDIRSAVE,mc._ADRPREFIX) 
        if _err != "0":
            return _err  
        
        _params = {"request": request, "mapsdir":_maps, "addrsdir":_adrs, "dir":mc._MAPDIRSAVE}
        return templates.TemplateResponse("mapresult.html",_params)
    except Exception as e:
        _params = {"request": request, "errlist": str(e),"logs":_mlog._LOG, "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)

@app.get("/kmlhtml", tags=['kmlhtml'])
#Импорт kml в html
async def get_kml(request: Request):
    _params = {"request": request}
    return templates.TemplateResponse("importKML.html",_params)

@app.post("/addpoint", tags=['addPoint'])
#Добавление точек пользователя
async def pointadd(request: Request):
    rform = await request.form()
    data = {'g_lat': rform['g_lat'].strip(),
            'g_lon': rform['g_lon'].strip(), 
            'poitname': rform['poitname'].strip(), 
            'mapname': rform['mapname'].replace("/","").strip(), 
    }
    res_code, res_msg = finddir.setPointDir(_START_DIR,data)
    return {"res_code": res_code, "res_msg" :res_msg }

@app.get("/foto{fotoname}/", response_class=HTMLResponse, tags=['editfoto'])
async def editfoto(request: Request, fotoname: str):
#Показ страницы с фотографиями
    try:   
        dirfoto = f"foto{fotoname}"
        dirgpx = f"gpx{fotoname}"
        app.mount(f"/{dirfoto}", StaticFiles(directory=f"./{dirfoto}"), name=dirfoto)
        res = mc.parseImageDir(dirfoto,loadInternet=False)
        userPoint = finddir.getPointNameDir(dirgpx)
        _params = {"request": request, "filesData": res["filesData"], "userPoint":userPoint}
        return templates.TemplateResponse("foto.html",_params)
    except Exception as e:
        _params = {"request": request, "errlist": str(e), "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)

@app.post("/foto{fotoname}/", response_class=HTMLResponse, tags=['savefoto'])
async def savefoto(request: Request, fotoname: str):
#Сохранение страницы с фотографиями
    try: 
        rform = await request.form()
        dirfoto = f"foto{fotoname}"
        mc.saveResultFotoSetting(dirfoto,rform)  
        return await editfoto(request,fotoname)
    except Exception as e:
        _params = {"request": request, "errlist": str(e), "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)

@app.post("/refer{namerefer}", tags=['reference'])
#Обработка справочников
async def refer(request: Request,namerefer:str):
    try: 
        if namerefer == "userpoint":
            rform = await request.form() 
            rname = rform["name"]
            rdir  = "GPX"+rform["dirname"][5:]
            _res =  finddir.getPointCoordFromNameDir(rdir,rname)
            if _res:
                _params = {"res": 0, "g_lat":_res["g_lat"],"g_lon":_res["g_lon"] } 
            else:    
                _params = {"res": 2, "error":f"Значение {rname} не найдено в {rdir}" } 
            return  _params  
        else:  
            _params = {"res": 1, "error": f"неизвестный справочник {namerefer}"}    
            return _params
        
    except Exception as e:
        _params = {"res": 1, "error": str(e), "tracelst":traceback.format_exc()}
        return _params
    
@app.post("/getplace", tags=['getplace'])
#Получение информации по координатам
async def getplace(request: Request):
    try: 
        rform = await request.form()
        lat = rform['g_lat'].strip()
        lon = rform['g_lon'].strip()
        res_code, poitname, resdict = geodata.geoPlace(lat,lon)
        return {"res_code": res_code, "poitname" :poitname, "resdict":resdict }    
    except Exception as e:
        _params = {"res_code": 1, "error": str(e), "tracelst":traceback.format_exc()}
        return _params
    
@app.post("/typeicon", tags=['reference'])
#Получение иконки по типу
async def typeicon(request: Request):
    try: 
        rform = await request.form()
        type = rform['type'].strip()
        a  = attr.reedAttr()
        icon = a[type].get("icon_icon")
        icon_prefix = a[type].get("icon_prefix")
        class_name  = f'{icon_prefix}-{icon}'
        _params = {"res": 0, "error":"","icon":icon,"icon_prefix":icon_prefix,"class_name":class_name } 
        return _params
    except Exception as e:
        _params = {"res": 1, "error": str(e), "tracelst":traceback.format_exc()}
        return _params
    
@app.get("/find", tags=['find'])
#Поиск по координатам
async def get_find(request: Request,degmode:str = "DEG", 
                   latitude:str="", longitude:str="",
                   latitudeDeg:str="", latitudeMin:str="", latitudeSec:str="",
                   longitudeDeg:str="", longitudeMin:str="", longitudeSec:str="" ):   
    def _fl(txt):
        if txt.strip() == "":
            return 0
        else:
            return float(txt.replace(',','.'))

    try:   
        errcode = '0'
        errtext = ""
        poitname = ""
        resdict  = {41}
        if degmode == "DM" and latitudeDeg and longitudeDeg:
           lat = mparse.dm_to_deg([_fl(latitudeDeg), _fl(latitudeMin), _fl(latitudeSec)])
           lon = mparse.dm_to_deg([_fl(longitudeDeg), _fl(longitudeMin), _fl(longitudeSec)])
        elif degmode == "DEG" and latitude and longitude:
            lat = _fl(latitude)                         
            lon = _fl(longitude) 
            latitudeDeg, latitudeMin, latitudeSec = mparse.decimal_to_degres(lat)
            longitudeDeg, longitudeMin, longitudeSec = mparse.decimal_to_degres(lon)
        else:
            errcode = '1'
            errtext = "Не заданы координаты для поиска" 

        if errcode =='0':
            latitude = str(lat)
            longitude = str(lon)
            errcode, poitname, resdict = geodata.geoPlace(lat,lon,True)

        _params = {"request": request, 
                   "degmode":degmode,
                   "latitude":latitude,
                   "longitude":longitude,
                   "latitudeDeg":latitudeDeg,
                   "latitudeMin":latitudeMin,
                   "latitudeSec":latitudeSec,
                   "longitudeDeg":longitudeDeg,
                   "longitudeMin":longitudeMin,
                   "longitudeSec":longitudeSec,
                   "poitname":poitname,
                   "resdict":resdict,
                   "errcode":errcode,
                   "errtext":errtext,
                   }
        return templates.TemplateResponse("find.html",_params)
    except Exception as e:
        _params = {"request": request, "errlist": str(e), "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("mapresultErr.html",_params)

@app.get("/trackEdit", tags=['trackEdit'])
#Редактор трека
async def get_editsetrack(request: Request):
    _params = {"request": request}
    return templates.TemplateResponse("trackEdit.html",_params)

@app.post("/trackEdit", tags=['trackEdit'])
#Разбор трека трека
async def get_parsetrack(request: Request, trackfile:Union[bytes, None] = File(default=None)):
    _mlog._LOG = []
    _mlog._PRINT_STATUS_ERR = True 
    try:   
        maplist  = []
        _err  = "0"
        rform = await request.form()
        finddir.load_env()   


        _fileObj = rform.get("trackfile")
        if _fileObj == None or _fileObj.filename == "":
            _err = "Не выбран файл для импорта"

        _params = {"request": request}    

        if _err != "0":  
            _params = {"request": request, "errcode":_err, "errlist": _err,"logs":_mlog._LOG}
            return templates.TemplateResponse("trackEdit.html",_params)  
        
        trackJs = {}
        tracks = {}
        filename = _fileObj.filename
        cnt, dataDict = mc.parseTrack(trackfile,filename)
        i = 0
        for track_ in dataDict:
            i+=1
            trackidx = f"track_id{i}"
            tracks[trackidx]=track_
            trackJs[trackidx]={"points":dataDict[track_],
                               "name":track_,
                               "newname":track_,
                               "visible":"1",
            } 
        trackJsStr = json.dumps(trackJs)

        #Вывод результата 
        _params = {"request": request,"logs":_mlog._LOG, "errcode":_err, "filename":filename,"dataDict":trackJsStr,"tracks":tracks}      
        return templates.TemplateResponse("trackEdit.html",_params)
    except Exception as e:
        _params = {"request": request, "errlist": str(e),"logs":_mlog._LOG, "tracelst":traceback.format_exc()}
        return templates.TemplateResponse("trackEdit.html",_params)    
    
@app.post("/exptrack", tags=['expTrack'])
#Экспорт трека в файл
async def exptrack(request: Request):
    res = "0"
    err = ""
    try:  
        rjson = await request.json()
        if rjson:
            cnt, gpxFilename = mc.creGpxDir(dir=mc._EXPGPX ,filename= rjson["filename"],tracks = rjson["tracksdir"],appication=_appication)
            if cnt == 0 :
                res = "1"
                err = "Пустой трек для экспорта"

        _params = {"res_code": res, "error": err, "files" : {"filegpx":gpxFilename,"filekml":"filename_kml"} }
        return _params
    except Exception as e:
        res = "2"
        _params = {"res_code": res, "error": str(e),"tracelst":traceback.format_exc()}
        return _params

