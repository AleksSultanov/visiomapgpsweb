""" VisioMapDemo
    version="2.0.0"
    Демо режим 
    Генерация иконок на карту.
    Генерация цветных линей на карту
   Результат в каталоге с картами, карта mapDemoIconColor.html
   
"""

import os
import folium
from folium.plugins import MousePosition
from folium import plugins

import mapLib.mapcfg as _cf
import mapLib.maplog as _mlog
import mapLib.mapAdd as _madd

_MAPNAME = "mapDemoIconColor"
_MAPNAMELINE = "mapDemoLineColor"
_MAPNAMEEMPTY = "mapDemoEmpty"
_MAPDIRSAVE = 'map'
_MAPSCRIPTFLIE = "../js/editMappoint.js"

_LON= 41.65
_LAT = 70.1
_ZOOM = 11
_MAPlAYER = {}            

def creMapIcon():
    resultlst   = []
    _mlog._save(f"Формируем демо карту {_MAPNAME}","info")
    _map = folium.Map(location=[_LON, _LAT],
                    tiles='openstreetmap', 
                    zoom_start=_ZOOM)

    for _m in _MAPlAYER:
        _dict = _MAPlAYER[_m]
        folium.TileLayer(_dict['tiles'], name=_m, attr=_dict['attr'], 
        min_zoom=_dict['min_zoom'],max_zoom=_dict['max_zoom'],
        subdomains=_dict['subdomains']
        ).add_to(_map)

    colors = _cf.colorlist()  
    icons  = _cf.iconslist()  
    _iconcount = 0            

    fg = folium.FeatureGroup(name='Иконки без заливки', show=False).add_to(_map)
    _c = 0
    _mlog._save("Создание иконок без заливки")
    for _color in colors:
        _c += 0.01
        _i = 0
        for _icon in icons:
            _iconcount += 1
            _i += 0.01
            folium.Marker(location=[_LON +_c, _LAT + _i], 
                        popup = f"<b>Цвет</b> {_color} <br> <b>Иконка</b> {_icon}", 
                        icon=folium.Icon(color=_color,
                                    icon_color=_color,
                                    icon=_icon,
                                    prefix="fa",
                                    icon_size = [8,8],
                                    shadow_size = [5,5]
                                    )
                        ).add_to(_map).add_to(fg)                      
    _mlog._save("Создание иконок с заливкой цветами")
    for _icon in icons:
        fg = folium.FeatureGroup(name=_icon, show=False).add_to(_map)
        _c = 0
        # _mlog._save(f"Иконка {_icon}")
        for _color in colors:
            _c += 0.01
            _i = 0
            for _icon_color in colors:
                _i += 0.01
                _iconcount += 1
                folium.Marker(location=[_LON +_c, _LAT + _i], 
                        popup = f"<b>Иконка</b> {_icon} <br> <b>Цвет</b> {_color} <br> <b>Цвет иконки</b> {_icon_color}", 
                        icon=folium.Icon(color=_color,
                                    icon_color=_icon_color,
                                    icon=_icon,
                                    prefix="fa"
                                    )
                        ).add_to(_map).add_to(fg) 

    _mlog._save(f"Панель полный экран")
    plugins.Fullscreen(position="topright",
        title="Просмотр на полном экране",
        title_cancel="Нажми для выхода",
        force_separate_button=True,
        ).add_to(_map)
    _mlog._save(f"Панель слоев")    
    folium.LayerControl(collapsed=True).add_to(_map)  
    _mlog._save(f"Позиции мышки")    
    MousePosition().add_to(_map)
    _mlog._save(f"Сохранение")    
    #сохраняем карту 
    if not os.path.exists(_MAPDIRSAVE):
        os.mkdir(_MAPDIRSAVE)
    mapFile = os.path.join(_MAPDIRSAVE,f"{_MAPNAME}.html")      
    _map.save(mapFile)
    _mlog._save(f"Создана демо карта {_MAPNAME}  с иконками {mapFile}","info")
    _row = {"mapname":_MAPNAME,"mapfile":mapFile,"countgpx":0,"countfoto":_iconcount}
    resultlst.append(_row)
    return resultlst

def creMapLine():
    resultlst   = []
    _mlog._save(f"Формируем демо карту {_MAPNAMELINE}","info")
    _map = folium.Map(location=[_LON, _LAT],
                    tiles='openstreetmap', 
                    zoom_start=_ZOOM)

    for _m in _MAPlAYER:
        _dict = _MAPlAYER[_m]
        folium.TileLayer(_dict['tiles'], name=_m, attr=_dict['attr'], 
        min_zoom=_dict['min_zoom'],max_zoom=_dict['max_zoom'],
        subdomains=_dict['subdomains']
        ).add_to(_map)

    colors = _cf.colorlist()  
    _linecount = 0   
    _c = 0         

    _mlog._save("Создание линий с заливкой цветами")
    for _color in colors:
        caption = f'<font color="{_color}" size="4">---</font> {_color}'
        fg = folium.FeatureGroup(name=caption, show=True).add_to(_map)
        _c += 0.01
        points = [(_LON+_c, _LAT),(_LON+_c, _LAT+0.2)]
        folium.PolyLine(points,
            color=_color,
            weight=4.5, 
            opacity=0.5
            ).add_to(_map).add_to(fg)         
        _linecount += 1    
        folium.Marker(location=[_LON +_c, _LAT], 
        popup = f"<b>Цвет</b> {_color} <br>", 
        icon=folium.Icon(color=_color,
                    icon_color="black",
                    icon="fire",
                    prefix="fa"
                    )
        ).add_to(_map).add_to(fg) 

    _mlog._save(f"Панель полный экран")
    plugins.Fullscreen(position="topright",
        title="Просмотр на полном экране",
        title_cancel="Нажми для выхода",
        force_separate_button=True,
        ).add_to(_map)
    _mlog._save(f"Панель слоев")    
    folium.LayerControl(collapsed=True).add_to(_map)  
    _mlog._save(f"Позиции мышки")    
    MousePosition().add_to(_map)
    _mlog._save(f"Сохранение")    
    #сохраняем карту 
    if not os.path.exists(_MAPDIRSAVE):
        os.mkdir(_MAPDIRSAVE)
    mapFile = os.path.join(_MAPDIRSAVE,f"{_MAPNAMELINE}.html")      
    _map.save(mapFile)
    _mlog._save(f"Создана демо карта {_MAPNAMELINE}  с иконками {mapFile}","info")
    _row = {"mapname":_MAPNAMELINE,"mapfile":mapFile,"countgpx":_linecount,"countfoto":0}
    resultlst.append(_row)
    return resultlst

def creMapEmpty():
    resultlst   = []
    _res        = "0"
    _mlog._save(f"Формируем демо карту {_MAPNAMEEMPTY}","info")
    _map = folium.Map(location=[_LON, _LAT],
                    tiles='openstreetmap', 
                    zoom_start=_ZOOM)
     
    _mlog._save(f"Позиции мышки")    
    MousePosition().add_to(_map)
    _mlog._save(f"Формирую html страциницу для карты")    
    root = _map.get_root()
    html = root.render()
    _mlog._save(f"Добавляю скрипт {_MAPSCRIPTFLIE}")
    html = _madd.addEditScript(html,_MAPSCRIPTFLIE)  
    _mlog._save(f"Сохранение карты на диск")        
    #сохраняем карту 
    if not os.path.exists(_MAPDIRSAVE):
        os.mkdir(_MAPDIRSAVE)
    mapFile = os.path.join(_MAPDIRSAVE,f"{_MAPNAMEEMPTY}.html")   
    # _map.save(mapFile)
    with open(mapFile, 'w', encoding='utf-8') as savefile:
            savefile.write(html)
    
    _mlog._save(f"Создана демо карта {_MAPNAMEEMPTY}  с иконками {mapFile}","info")
    _row = {"mapname":_MAPNAMEEMPTY,"mapfile":mapFile,"countgpx":0,"countfoto":0}
    resultlst.append(_row)
    return resultlst, _res