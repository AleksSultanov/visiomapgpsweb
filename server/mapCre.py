""" mapCre
    version="2.0.0"
    Модуль с методами для создания карты визуализации треков
    Для модуля visioMap
"""
from datetime import datetime
import time
import os
import csv
import folium
from folium.plugins import MousePosition
from folium.plugins import MiniMap
from folium import plugins
import copy

from mapLib.mapparse import parseGPX, struct_imageFull, parseImage, parseKmlFolder, expKmlStrHtml, parseGPXobj, parseKmlobj
import mapLib.mapAdd as _madd
import mapLib.reqYandex as yandx
import mapLib.finddir as finddir
import mapLib.mapcfg as attr
import mapLib.maplog as _mlog

_MAPDIRSAVE = 'expMap'
_ADRPREFIX = 'adr'
_HTMLEXT   = '.html'
_GPXCSV     = False  #Формировать csv файл с расшифровкой вычислений gpx
_STATUS_OK  = 200
_STATUS_OK2 = 201
_MAPSCRIPTFLIE = "../js/editMappoint.js"
_FOTOSETTING = "result.csv"
_EMPTYTXTPOINT = "Установить координаты из точки пользователя:"
_EXPGPX = 'expFile'  
_ECODING = 'utf-8'      

#Функции для загрузки в интернет
def uploaDisk(file):
    status_code, err_text = yandx.uploaDisk(file)
    return status_code, err_text

def readDisk(filesData):
    res_filesLink = {}
    res_filesData = []
    status_code, err_text = yandx.publicDisk()
    if status_code == _STATUS_OK:
        status_code, resData ,err_text = yandx.readDisk()
        if status_code == _STATUS_OK:
            for data in resData:
                if "public_url" in data.keys() and "name" in data.keys():
                    _link  = data["public_url"]                    
                    _prev  = f"https://getfile.dokpub.com/yandex/get/{_link}" 
                    row = {"preview":_prev,"link":_link}    
                    res_filesLink.setdefault(data["name"],row)
            if len(res_filesLink) == 0:
                return filesData, status_code, err_text    
            sDate = str(datetime.now()) 
            for _data in filesData:
                if _data["file"] in res_filesLink.keys():
                    _data["link"] = res_filesLink[_data["file"]]["link"]
                    _data["preview"] = res_filesLink[_data["file"]]["preview"]
                    _data["udateinternet"] = sDate
                # res_filesData.append(_data)  
    return filesData, status_code, err_text   

# Функции для разбора фото в каталоге
def getCashe(file,existsfilesData):
    result = struct_imageFull().copy()
    result["file"] = file
    for old_row in existsfilesData:
        if old_row["file"] == file:
            return old_row
    return result        

def parseImageDir(map_Dir, issave:bool=True, loadInternet:bool=True):
    result = {"count":0,"uploadcount":0}
    upCnt = 0
    fileCsv =os.path.join(map_Dir,_FOTOSETTING)
    if not 'win_excel' in csv.list_dialects():
        csv.register_dialect('win_excel', delimiter=';', quoting=csv.QUOTE_NONE)
    start_time = time.time() 
    if not os.path.isdir(map_Dir):
        print(f'Нет каталога по указанному пути "{map_Dir}"')
        return result
    lstdir  = os.listdir(map_Dir) 
    filesData = []
    existsfilesData = []
    if os.path.exists(fileCsv):
        with open(fileCsv,'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile,dialect='win_excel')
            for data in reader:
                existsfilesData.append(data)
    
    for l in lstdir:
        file = os.path.join(map_Dir,l)
        if file == fileCsv:
            continue
        #сначала смотрим старый результат
        _row = getCashe(file,existsfilesData)
        if _row ["udategps"] != None :
            filesData.append(_row) 
        else:
            _gprrow = parseImage(file)
            for key in _gprrow:
               _row[key] = _gprrow[key] 
            filesData.append(_row)
    gpx_time = time.time()       
    dur_gpx_time = round(gpx_time -  start_time,2) 

    if loadInternet:
        #Отправка на сервер
        ifUpload = False
        for urldate in filesData:
            file  = urldate["file"]
            _row = getCashe(file,existsfilesData)
            if _row["link"] == None or _row["link"].strip() == "":
                _mlog._save(f"Загрузка на сервер файла {file}") 
                status_code,err_text = uploaDisk(file)  
                if status_code in [_STATUS_OK,_STATUS_OK2]:
                    upCnt += 1              
                    ifUpload = True
                else:
                    _mlog._save(f"Ошибка загрузки на сервер файла {file}, ошибка: [{status_code}] {err_text}","error")
        # ifUpload = True       
        if ifUpload:   
            _mlog._save("Публикация и обновление ссылок на фотографии . .")
            filesData , status_code, err_text  = readDisk(filesData)  
            if status_code != _STATUS_OK:
                _mlog._save(f"Ошибка публикации и обновления ссылок на фотографии, ошибка: [{status_code}] {err_text}","error")     

    cnt =len(filesData) 
    if issave:
        fieldnames = filesData[0].keys()
        with open(fileCsv, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                                    extrasaction='ignore',
                                    dialect='win_excel')
            writer.writeheader()
            for _row in filesData:
                writer.writerow(_row)
   
    dur_upp_time = round(time.time() -  gpx_time,2)
    dur_all_time = round(time.time() -  start_time,2)
    for _row in filesData:
        if _row["latitude"] and _row["longitude"]:       
            _row["mapsUrl"] = _madd.mapsUrl(_row["latitude"] , _row["longitude"])  
        else: 
            _row["mapsUrl"] = ""

    result = {"count":cnt,"uploadcount":upCnt,
              "filesData":filesData,
              "dur_gpx_time":dur_gpx_time,
              "dur_upp_time":dur_upp_time,
              "dur_all_time":dur_all_time
             } 
    return result

#Для тестов
def tofile(fname,list):
    if not 'win_excel' in csv.list_dialects():
        csv.register_dialect('win_excel', delimiter=';', quoting=csv.QUOTE_NONE)
    fieldnames = list[1].keys()
    with open(fname+'.csv', 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                 extrasaction='ignore',
                 dialect='win_excel')
        writer.writeheader()
        for _row in list:
                writer.writerow(list[_row])    


def runCreMap(a,fsettings,MapDirectory):
    _ZOOM_START = int(fsettings['zoom'])
    _FOTO_ON    = bool(fsettings['fotoOn'])
    _AUTO_ON    = bool(fsettings['autoOn'])
    _WHEATHER   = bool(fsettings['wheatherShow'])
    _MOUSEPOSON = bool(fsettings['mouseposOn'])
    _MINIMAP    = bool(fsettings['minimapOn'])
    _MAPlAYER   = fsettings['MapLayerbox']
    _TZHOUR     = int(fsettings['tzh'])
    _TZMINUTE   = int(fsettings['tzmin'])
    CountDir    = {"gpx":0,"foto":0,"map":0,"gpxM":0,"fotoM":0}
    resultlst   = []
    _res        = "0"
    
    root_time = time.time() 

    if _TZHOUR !=0 : 
        a["timezone"]["hours"] = _TZHOUR
    if _TZMINUTE !=0 :     
        a["timezone"]["minutes"] = _TZMINUTE

    for _mapName in MapDirectory:
        _map = folium.Map(location=None,tiles=None,zoom_start=10)
        weather_locations = []
        foto_locations = []
        CountDir["map"] += 1
        CountDir["gpxM"]  = 0
        CountDir["fotoM"] = 0

        mapObjectList = []
        for _dirName in MapDirectory[_mapName]:
            typelist = MapDirectory[_mapName][_dirName]["files"].keys()
            for _type in typelist:
                if _type == "foto":
                    _mlog._save(f"обработка фото в папке {_dirName} ")
                    res_parse = parseImageDir(_dirName)
                    foto_locations = res_parse["filesData"]
                    f_cnt =  res_parse["count"]
                    f_upcnt =  res_parse["uploadcount"]
                    f_time = res_parse["dur_gpx_time"]
                    f_itipme = res_parse["dur_upp_time"]
                    CountDir["fotoM"] =  f_cnt
                    _mlog._save(f"Обработано {f_cnt} фото. Загруженно на сервер {f_upcnt}. Время обработки :{f_time}, время загрузки: {f_itipme}","info")
                    for row in foto_locations:
                        _lat = row["latitude"]
                        _lon = row["longitude"]
                        startlocation = [_lat, _lon]
                        break
                    continue
                cnt_type  = len(MapDirectory[_mapName][_dirName]["files"][_type])
                groupfl = (cnt_type > 2)
                #Если файлов по категории больше 2, создадим группу для FeatureGroup
                if groupfl:
                    isShow = not (_type == 'auto' and not _AUTO_ON and ('empty' in typelist or 'man' in typelist) )
                    captionGr = _madd.getCaptionGr(a,_type)
                    fgg = folium.FeatureGroup(name=captionGr, show=isShow)
                    mapObjectList.append(fgg)
                fglist = {}
                isKML = False    
                for _file in MapDirectory[_mapName][_dirName]["files"][_type]:
                    af = copy.deepcopy(a)
                    if _file.get("line_color") and a.get(_file.get("type")):
                        af[_file.get("type")]["line_color"] = _file.get("line_color")

                    if _file.get("icon_color") and a.get(_file.get("type")):
                        af[_file.get("type")]["icon_color"] = _file.get("icon_color")
                        af[_file.get("type")]["circle_color"] = _file.get("icon_color")

                    if _file.get("line_weight") and a.get(_file.get("type")):
                        af[_file.get("type")]["line_weight"] = _file.get("line_weight")

                    file = os.path.join(_dirName, _file["file"])
                    dataDict = {}
                    caption = _madd.getCaption(af,_file)
                    _mlog._save(f"парсинг файла {file} ")
                    if file[-3::].upper() == 'GPX':
                        dataDict = parseGPX(file)
                    if file[-3::].upper() == 'KML':
                        dataDict = parseKmlFolder(file)  
                        isKML = True
                    if len(dataDict) != 0:
                        for _key in dataDict: 
                            result, locations = dataDict[_key] 
                            if isKML:  
                                sPrintFile = f"{file} - {_key}"
                            else:    
                                sPrintFile = file
                            _mlog._save(f"построение трека {sPrintFile} ")
                            if _GPXCSV:
                                tofile(_file["file"][:-4].strip(),locations)
                            if result == 0:
                                _mlog._save(f"Пустой трек {sPrintFile}","error")
                                continue
                            CountDir["gpx"]  += 1  
                            CountDir["gpxM"] += 1     
                            #Создание FeatureGroup\FeatureGroupSubGroup для каждого файла
                            if isKML: 
                                caption = _key       
                            isExistFg = False
                            for key in fglist:
                                if key == caption:
                                    isExistFg = True
                                    fg = fglist[key]
                            if not isExistFg:
                                if groupfl:
                                    fg = plugins.FeatureGroupSubGroup(fgg,caption)
                                else:    
                                    fg = folium.FeatureGroup(name=caption, show=True)
                                fglist.setdefault(caption,fg)
                                mapObjectList.append(fg)  
                            startlocation =[ locations[1]["lat"],locations[1]["lon"] ]
                            maxPoint = len(locations)
                            Finishlocation = [ locations[maxPoint]["lat"],
                                                locations[maxPoint]["lon"] ]  
                            if _WHEATHER:                                     
                                weather_locations.append(startlocation)
                                weather_locations.append(Finishlocation)
                            locatDict = copy.deepcopy(_file)
                            
                            locatDict.setdefault("locations",locations)
                            #Рисуем трек
                            _madd.trackPoint(af,_map,locatDict,fg) 
                            #Рисуем интервальные точки 
                            _madd.intervalPoint(af,_map,locatDict,fg)

        if CountDir["gpxM"] == 0 and CountDir["fotoM"] == 0 :
            _mlog._save(f"Нет данных для создания карты {_mapName}","error")
        else:  
            #создание карты
            _map = folium.Map(location=startlocation,
                tiles=None,
                zoom_start=_ZOOM_START)  
            #Слои карты         
            folium.TileLayer('OpenStreetMap', name="OpenStreet Map").add_to(_map)
            for _m in _MAPlAYER:
                _dict = _MAPlAYER[_m]
                folium.TileLayer(_dict['tiles'], name=_m, attr=_dict['attr'], 
                min_zoom=_dict['min_zoom'],max_zoom=_dict['max_zoom'],
                subdomains=_dict['subdomains']
                ).add_to(_map)

            if len(weather_locations) >0 :
                captionW = _madd.getCaptionGr(a,"weather")   
                wfg = folium.FeatureGroup(name=captionW, show=False).add_to(_map)        
                _madd.addWeather(a,_map,weather_locations,wfg)
            if len(foto_locations) >0 :
                captionF = _madd.getCaptionGr(a,"foto")  
                fotofg = folium.FeatureGroup(name=captionF, show=(CountDir['gpxM']==0 or _FOTO_ON)).add_to(_map)        
                _madd.addFotos(a,_map,foto_locations,fotofg)

            for _obj in mapObjectList:
                _map.add_child(_obj)
            
            mapFile = _mapName + _HTMLEXT
            # Добавление панелей на карту"            
            plugins.Fullscreen(position="topright",
                title="Просмотр на полном экране",
                title_cancel="Нажми для выхода",
                force_separate_button=True,
                ).add_to(_map)
            folium.LayerControl(collapsed=True).add_to(_map)  
            if _MOUSEPOSON:
                _mlog._save(f"Координаты позиции курсора мышки")    
                MousePosition().add_to(_map)
            if _MINIMAP:
                _mlog._save(f"Миникарта")    
                _minimap = MiniMap(tile_layer="Stamen Terrain") 
                _map.add_child(_minimap)  
            _mlog._save(f"Формирую html страциницу для карты")    
            root = _map.get_root()
            html = root.render()
            _mlog._save("Добавляю скрипты")
            html = _madd.addEditScript(html,_MAPSCRIPTFLIE)  
            _mlog._save(f"Сохранение карты на диск")        
            #сохраняем карту 
            if not os.path.exists(_MAPDIRSAVE):
                os.mkdir(_MAPDIRSAVE)
            mapFile = os.path.join(_MAPDIRSAVE,mapFile)      
            # _map.save(mapFile)
            with open(mapFile, 'w', encoding=_ECODING) as savefile:
                    savefile.write(html)

            _row = {"mapname":_mapName,"mapfile":mapFile,"countgpx": CountDir['gpxM'],"countfoto":CountDir['fotoM']}
            resultlst.append(_row)
            _mlog._save(f"Создана карта {mapFile}, кол-во треков: {CountDir['gpxM']}, фотографий: {CountDir['fotoM']}","info")
            
    finish_time = round(time.time() - root_time,2)
    _mlog._save(f"Обработано треков: {CountDir['gpx']}, создано карт: {CountDir['map']}, время на все операции {finish_time} сек","info")
    return resultlst, _res 

# Копирование настроек с запроса формы _form в словарь файлов карты
def mapSettinglst(_mapfilelist,_form):
    def copyfrm(_mapprm, _formdata,_fkey):
        if _formdata.get(_fkey) != None:
            if _formdata.get(_fkey).strip() != "":
                return _formdata[_fkey]
        if _mapprm != None:
            return _mapprm        
        else:
            return "empty"

    # Формирую только отмеченные карты
    mapfilelist = {}
    
    for _map in _mapfilelist:
        if _map+"_ch" in _form.keys():
            mapfilelist[_map] = _mapfilelist[_map]
            for _d in _mapfilelist[_map]:
                mapfilelistKey = {}
                for _t in _mapfilelist[_map][_d]["files"]:
                    for _f in _mapfilelist[_map][_d]["files"][_t]:
                        _keyf = _f["file"]
                        if _keyf in _form.keys():
                            idx = _form[_keyf]
                            _f["name"]= copyfrm(_f.get("name"),_form,idx+"_name")
                            _f["date"]= copyfrm(_f.get("date"),_form,idx+"_date")
                            _f["type"]= copyfrm(_f.get("type"),_form,idx+"_type")
                            _f["icon_color"]= copyfrm(_f.get("icon_color"),_form,idx+"_iconcolor")
                            _f["line_weight"]= copyfrm(_f.get("line_weight"),_form,idx+"_weight")

                            _linecolor = copyfrm(_f.get("line_color"),_form,idx+"_color")
                            if _linecolor in attr.colorlinelist():
                                _f["line_color"] = _linecolor
                        mapfilelistKey[_keyf] = _f         
                _dirpath = os.path.join(_mapfilelist[_map][_d]["rootdir"],_d)
                _dirtype = _mapfilelist[_map][_d]["dirtype"]
                if os.path.exists(_dirpath) and _dirtype == "GPX":
                    fileSetGpx = os.path.join(_dirpath,finddir._FILESETGPX) 
                    with open(fileSetGpx, 'w') as savefile:
                        savefile.write(str(mapfilelistKey))                        
    return mapfilelist        

def runCreAdrKml(kml_obj:bytes,file_name):
    if file_name[-3::].upper() != 'KML':
        return ["",], f"Обработка только KML файлов, текущий: {file_name}"
    adrName = _ADRPREFIX+file_name[0:-4:]
    kml_str = kml_obj.decode(_ECODING.upper())    
    htmltext, err  = expKmlStrHtml(kml_str,file_name)
    adrFile = adrName+_HTMLEXT
    adrPathFile = os.path.join(_MAPDIRSAVE,adrFile)   
    if err == "0":
        if not os.path.exists(_MAPDIRSAVE):
            os.mkdir(_MAPDIRSAVE)
        with open(adrPathFile, 'w', encoding=_ECODING) as savefile:
            savefile.write(htmltext)
    return [adrName,], err

    
def convertFotoData(formdata):
    files = {}
    for f in formdata:
        if f.find("filename_") == 0:
            id = f[9:] 
            keylat = f"latitude_{id}"
            keylon = f"longitude_{id}"
            keydescr = f"description_{id}"
            keypoint = f"userPoint_{id}"
            files[formdata.get(f)] = {"id":id,
                                      "latitude":formdata.get(keylat), 
                                      "longitude":formdata.get(keylon), 
                                      "description":formdata.get(keydescr), 
                                      "userPoint":formdata.get(keypoint), 
            }  
    return files

def saveResultFotoSetting(map_Dir, formdata):
    fileCsv =os.path.join(map_Dir,_FOTOSETTING)
    if not 'win_excel' in csv.list_dialects():
        csv.register_dialect('win_excel', delimiter=';', quoting=csv.QUOTE_NONE)
    if not os.path.exists(fileCsv):
        return 1, f'Нет файла {fileCsv}'
    formfile = convertFotoData(formdata)    
    fotoData = []
    NewData = []
    with open(fileCsv,'r', newline='') as csvfile:
        reader = csv.DictReader(csvfile,dialect='win_excel')
        for data in reader:
            fotoData.append(data)

    for _row in fotoData:
        if _row.get("userPoint") == None:
            _row.setdefault("userPoint","")
        _file = _row.get("file")     
        if _file in formfile.keys():
            _row["latitude"] = formfile[_file].get("latitude")
            _row["longitude"] = formfile[_file].get("longitude")
            _row["description"] = formfile[_file].get("description")
            if formfile[_file].get("userPoint") !=None and formfile[_file].get("userPoint").find(_EMPTYTXTPOINT) == 0:
                _row["userPoint"]  = ""
            else:
                _row["userPoint"] = formfile[_file].get("userPoint")
        NewData.append(_row)    

    fieldnames = NewData[0].keys()
    with open(fileCsv, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                                extrasaction='ignore',
                                dialect='win_excel')
        writer.writeheader()
        for _row in NewData:
            writer.writerow(_row)
   
    return 0, "ok"

def parseTrack(fileobj, filename):
    _mlog._save(f"парсинг файла {filename} ")
    _result = {}
    result = {}
    tracks = {}
    res = 0

    if filename[-3::].upper() == 'GPX':
        _result = parseGPXobj(fileobj,filename)
    if filename[-3::].upper() == 'KML':
        styledict, styleMapdict,folderDict = parseKmlobj(fileobj)  
        for _n in folderDict:
            for _p in folderDict[_n]:
                _val = folderDict[_n][_p]["tracks"]
                _result[_p] = len(_val),_val
    for track in _result:
        _res, tracks = _result[track]
        res += _res
        result[track] ={}
        for key in tracks:
            _time=tracks[key].get("time")
            timeS = ""
            if _time != None and _time != 0:
                timeS = _time.strftime('%Y-%m-%dT%H:%M:%SZ')
            result[track][str(key)] = {"time":timeS,
                        "lat":tracks[key].get("lat"),
                        "lon":tracks[key].get("lon"),
                        "ele":tracks[key].get("ele"),
                        } 

    return res, result 


def creGpxDir(dir:str,filename:str,tracks,appication="VisioMap") :
    gpxtxt = f"<?xml version='1.0' encoding='{_ECODING.upper()}' standalone='yes' ?>\n"
    gpxtxt += f'<gpx version="1.0" creator="{appication}" '
    gpxtxt += 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
    gpxtxt += 'xmlns="http://www.topografix.com/GPX/1/0" xsi:schemaLocation="http://www.topografix.com/GPX/1/0/gpx.xsd">\n'
    cnt = 0
    gpxFile = ''
    body    = ''
    if filename[-3::].upper() != 'GPX':
        if filename[-4:-3:1].upper() == '.':
            filename = filename[:-3:]+'gpx'
        else:
            filename = filename+'.gpx'    

    for tr in tracks :
        track= tracks[tr]
        if track["visible"] == "0" :
            continue
        body += f" <trk><name>{track['newname']}</name>\n"
        body += "  <trkseg>\n"
        poitns = track['points']
        for point in poitns :
            row = poitns[point]
            if row["mod"] == "d" :
                continue
            cnt += 1
            _lat = row["lat"]
            _lon = row["lon"]
            _ele = row["ele"]
            _time = row["time"]
            body += f'   <trkpt lat="{_lat}" lon="{_lon}"><ele>{_ele}</ele><time>{_time}</time></trkpt>\n'

        body += '  </trkseg>\n</trk>\n'
    gpxtxt += body
    gpxtxt += '</gpx>'

    if cnt > 0:
        if not os.path.exists(dir):
            os.mkdir(dir)
        gpxFile = os.path.join(dir,filename)      
        with open(gpxFile, 'w', encoding=_ECODING) as savefile:
            savefile.write(gpxtxt)

    return cnt, gpxFile